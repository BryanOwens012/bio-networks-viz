// This component contains the static text at the top of the page describing the project and instructions


import React, {Component} from 'react';


class Instructions extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    };

    componentDidUpdate() {
    };

    render() {
        return (
            <div>
                <h1>Biological Co-Expression Network Visualizer</h1>
                <h2>Senior Project by <a href="https://www.linkedin.com/in/bryanowens012/" target="_blank" style={{"color": "#0f4d92"}}>Bryan Owens</a>, B.S. in computer science at Yale, May 2021</h2>
                <div>
                    <h4>About</h4>
                    <p>Many microRNAs have a strong co-expression relationship with each other, meaning they synthesize proteins similarly — or more generally, their effects on organisms are correlated.
                        This web app lets you visualize such co-expression correlations between microRNAs.</p>
                    <p>Although there are existing gene co-expression network (GCN) visualizers, my goal was to build my own interactive and user-friendly interface.</p>
                    <p>This app is hosted on Firebase (Google Cloud).
                        It is preloaded with a microRNA co-expression network created from real cancer data, courtesy of my classmate Ciaran Hassan's Senior Thesis (<i>A Network-based Approach for the Derivation of
                        Prognostic microRNA Signatures and Oncological Risk Scores</i>, by Hassan, Kumar, Lin, and Gerstein).</p>
                    <h4>Instructions</h4>
                    <ol>
                        <li>
                            Select names from the "Select microRNAs" dropdown menu to see them visualized in the graph network.
                            Try not to select all names, because that's computationally intensive and can freeze the app.
                        </li>
                        <li>
                            Change the "Correlation cutoff" to a positive number to change the cutoff that determines whether two microRNAs are considered to have a strong co-expression relationship.
                            Any correlation between two microRNAs where |correlation| > cutoff is considered significant and gets visualized as a constant-width edge between the two microRNAs (nodes) in the graph network.
                        </li>
                        <li>
                            Hover your mouse over individual nodes (microRNAs) in the graph network to see their correlation data appear in the right panel of the table.
                        </li>
                        <li>
                            Use your mouse's scroll wheel to zoom in and out of the graph network. This feature is very buggy but hopefully still helps.
                        </li>
                        <li>
                            If there's any issue, reload the page to reset and unfreeze the app.
                        </li>
                    </ol>
                </div>
                <br />
                <hr />
            </div>
        );
    }
}

export default Instructions;