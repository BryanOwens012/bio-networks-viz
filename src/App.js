// The main component, and the one that is automatically loaded first

// import './dashboard.css';
import React, { useState } from 'react';
import MultiSelect from 'react-multi-select-component';
import * as d3 from 'd3';
import { TextField } from '@material-ui/core/';

import genes from './genes.json';
import Graph from './Graph';
import InfoPanel from './InfoPanel';
import './App.css';
import Instructions from "./Instructions";

// If you want to create a sample dataset for testing
function createSampleDataset() {
    const gene_names = [...Array(26).keys()].map(i => String.fromCharCode(i + 97));
    // const gene_names = [...Array(1000).keys()];
    var corrs = createCorrs(gene_names);
    const options = gene_names.map((letter) => Object({label: letter, value: letter}));

    function createCorrs(gene_names) {
        var corrs = {};

        for (let i = 0; i < gene_names.length; i++) {
            for (let j = i + 1; j < gene_names.length; j++) {
                if (! (gene_names[i] in corrs)) {
                    corrs[gene_names[i]] = {};
                }
                corrs[gene_names[i]][gene_names[j]] = Math.random();
            }
        }

        return corrs;
    }

    return [gene_names, corrs, options];
}

// To help with scoping
function createDataset() {
    return [genes.gene_names, genes.corrs, genes.options];
}


// Pretty-print the genes that the user has selected
function printSelected(selected) {
    if (selected.length == 0) {
        return <div><b>[None]</b></div>;
    }
    if (selected.length < 20) {
        return <div>{selected.map((gene) => gene['label']).join(', ')}</div>;
    }
    if (selected.length > 20) {
        return <div><div>{selected.slice(0, 20).map((gene) => gene['label']).join(', ')}</div><div><b>+ {selected.length - 20} more ...</b></div></div>;
    }
}


function App() {


    const [gene_names, corrs, options] = createDataset();
    // const [gene_names, corrs, options] = createSampleDataset();

    const [selected_genes, setSelectedGenes] = useState([]);
    const [corr_cutoff, setCorrCutoff] = useState(0.8); // Set default corr cutoff to 0.8
    const [info_panel_info, setInfoPanelInfo] = useState({});

    return (
        <div style={{marginLeft: '5%'}}>

            <Instructions />

            <div style={{width: '50%'}}>
                <h3>Select microRNAs:</h3>
                <MultiSelect
                    options={options}
                    value={selected_genes}
                    onChange={setSelectedGenes}
                    labelledBy={'Select'}
                />
            </div>
            <br />

            <b>Selected microRNAs ({selected_genes.length}):</b>
            <br />
            <br />
            {printSelected(selected_genes)}

            <br />
            <br />
            <br />
            {/*https://blog.logrocket.com/a-guide-to-usestate-in-react-ecb9952e406c/*/}
            <TextField id="outlined-basic" label="Correlation cutoff" variant="outlined" onChange={(e) => setCorrCutoff(e.target.value)} defaultValue={0.8} />

            <br />
            <br />

            {/*https://material-ui.com/components/text-fields/#text-field*/}
            <table style={{width: '100%', border: '2px solid black', marginRight: '10%'}}>
                <tr>
                    <td style={{width: '60%', border: '2px solid black'}}>
                        {/*https://medium.com/@jasminegump/passing-data-between-a-parent-and-child-in-react-deea2ec8e654*/}
                        <Graph genes={selected_genes} corrs={corrs} corr_cutoff={corr_cutoff} callback={setInfoPanelInfo} />
                    </td>
                    <td style={{width: '50%', border: '2px solid black'}}>
                        <InfoPanel info_panel_info={info_panel_info} />
                    </td>
                </tr>
            </table>

            <br />
            <br />

        </div>
    );
};

export default App;