// This component displays the correlation data for a single gene

import React, {Component} from 'react';

class InfoPanel extends React.Component {
    constructor(props) {
        super(props);
        this.info_panel_info = props.info_panel_info;
    }

    componentDidMount() {
        console.log(`InfoPanel mounted!: ${this.info_panel_info["gene_name"]}`);
    }

    componentDidUpdate() {
        console.log('InfoPanel.componentDidUpdate() called: ' + JSON.stringify(this.props.info_panel_info));
        this.info_panel_info = this.props.info_panel_info;
    }

    writeGeneInfo(info_panel_info = this.info_panel_info) {

        if (Object.keys(info_panel_info).length === 0) {
            return <h1>Mouse over a node to see its microRNA info.</h1>;
        }
        else {
            // Sort by correlation, descending
            let genes = Object.entries(info_panel_info.gene_corrs);
            genes.sort((elt0, elt1) => elt1[1] - elt0[1]);

            // Create rows
            let rows = genes.map((key, value) =>
                <tr>
                    <td style={{'padding': '0 20px'}}>{key[0]}</td>
                    <td style={{'padding': '0 20px'}}>{Math.round(key[1] * 10000) / 10000}</td>
                </tr>
            );

            let gene_corrs_length = Object.keys(info_panel_info.gene_corrs).length;

            return (
                <div>
                    <table style={{'marginTop': '0'}}>
                        <tr>
                            <th colspan="2">
                                <h1>{info_panel_info.gene_name}</h1>
                                <h4>Found {gene_corrs_length} microRNA{gene_corrs_length > 1 ? "s" : ""} that ha{gene_corrs_length > 1 ? "ve" : "s"} a significant co-expression relationship with {info_panel_info.gene_name}:</h4>
                            </th>
                        </tr>
                        <tr>
                            <td style={{'padding': '0 20px'}}><b>microRNA name</b></td>
                            <td style={{'padding': '0 20px'}}><b>Correlation</b></td>
                        </tr>
                        <tr>
                            <td colspan = "2">
                                {/* Enable scrollbar */}
                                <div style={{'overflow': 'scroll', 'height': '500px'}}>
                                    <table>
                                        {rows}
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            )
        }
    }

    render() {
        return (
            <div id={"#" + this.props.id} style={{'marginLeft': '5%', 'marginRight': '5%'}}>
                {this.writeGeneInfo(this.props.info_panel_info)}
            </div>
        );
    }

}

export default InfoPanel;