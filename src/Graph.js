// This component dynamically recalculates the graph's nodes and edges whenever the user selects different genes
// or different correlation cutoffs

// https://bl.ocks.org/heybignick/3faf257bbbbc7743bb72310d03b86ee8
// https://blog.logrocket.com/data-visualization-in-react-using-react-d3-c35835af16d0/
// https://stackoverflow.com/questions/13615381/d3-add-text-to-circle

import React, {Component} from 'react';
import * as d3 from 'd3';
// import './Graph.css'

const CORR_MAX = 1;
const CORR_NOT_SET_FLAG = -2;

class Graph extends React.Component {
    // https://reactjs.org/docs/state-and-lifecycle.html
    constructor(props) {
        super(props);
        this.genes = props.genes;
        this.corrs = props.corrs;
        this.corr_cutoff = props.corr_cutoff;
        this.callback = props.callback;
        this.svg = null;
    }

    componentDidMount() {
        this.eraseGraph();
        this.createGraph();
    }

    // When the genes, correlations, or correlation cutoff change, update the graph elements and redraw the graph
    // https://reactjs.org/docs/react-component.html
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.genes == this.props.genes && prevProps.corrs == this.props.corrs
            && prevProps.corr_cutoff == this.props.corr_cutoff && prevProps.callback == this.props.callback) {
            console.log("componentDidUpdate() called, but no updates detected");
            return;
        }

        console.log("componentDidUpdate() called");
        this.genes = this.props.genes;
        this.corrs = this.props.corrs;
        this.corr_cutoff = this.props.corr_cutoff;
        this.callback = this.props.callback;
        console.log(`this.corr_cutoff = ${this.corr_cutoff}`);

        if (this.corr_cutoff == 0) {
            return;
        }

        this.genes_graph = {};
        this.genes_graph.nodes = this.genes.map((gene) => Object({name: gene.label}));
        this.genes_graph.nodes_set = new Set(this.genes.map((gene) => gene.label)); // to achieve O(1) find in this.handleCallback()
        this.genes_graph.edges = [];

        for (let i = 0; i < this.genes_graph.nodes.length; i++) {
            for (let j = i + 1; j < this.genes_graph.nodes.length; j++) {
                let gene_name_i = this.genes_graph.nodes[i].name;
                let gene_name_j = this.genes_graph.nodes[j].name;
                let corr = CORR_NOT_SET_FLAG;
                let present_in_i = false,
                    present_in_j = false;

                // Is gene j present in gene i's correlations dict?
                if ((gene_name_i in this.corrs)
                    && (gene_name_j in this.corrs[gene_name_i])) {
                    corr = this.corrs[gene_name_i][gene_name_j];
                    present_in_i = true;
                }

                // Is gene i present in gene j's correlations list?
                if ((gene_name_j in this.corrs)
                    && (gene_name_i in this.corrs[gene_name_j])){
                    corr = this.corrs[gene_name_j][gene_name_i];
                    present_in_j = true;
                }

                // If gene j is present in i's corrs list but gene i isn't present in gene j's corr list,
                // add gene i to gene j's corr list
                if (present_in_i && !present_in_j) {
                    if (! (gene_name_j in this.corrs)) {
                        this.corrs[gene_name_j] = {};
                    }
                    this.corrs[gene_name_j][gene_name_i] = corr;
                }

                // If gene i is present in j's corrs list but gene j isn't present in gene i's corr list,
                // add gene j to gene i's corr list
                if (present_in_j && !present_in_i) {
                    if (! (gene_name_i in this.corrs)) {
                        this.corrs[gene_name_i] = {};
                    }
                    this.corrs[gene_name_i][gene_name_j] = corr;
                }

                // If the corr was found in either gene's corrs list and the corr exceeds the cutoff,
                // then add it to the graph as an edge
                if ((corr != CORR_NOT_SET_FLAG) && (Math.abs(corr) > this.corr_cutoff)) {
                    this.genes_graph.edges.push(Object({source: i, target: j, corr: corr}));
                    // console.log(`Pushed ${gene_name_i} => ${gene_name_j}, with corr ${corr}`);
                }
            }
        }

        this.eraseGraph(this.svg);
        this.drawGraph(this.genes_graph);
    }

    // Tell the parent component which gene the user has moused over
    handleCallback(idx, nodes = this.genes_graph.nodes, nodes_set = this.genes_graph.nodes_set, corrs = this.corrs, corr_cutoff = this.corr_cutoff) {
        let gene_name = nodes[idx]["name"];

        // https://stackoverflow.com/a/56081419
        let gene_corrs_filtered = Object.fromEntries(
            Object.entries(corrs[gene_name]).filter(
                ([key, val]) => (nodes_set.has(key) && Math.abs(val) > corr_cutoff)
            )
        );

        this.callback({gene_name: gene_name, gene_corrs: gene_corrs_filtered});
    }

    // Init the graph's core elements
    createGraph() {
        //Width and height
        // this.w = (2/3) * window.innerWidth;
        this.w = 0.6 * window.innerWidth;
        this.h = 700;
        this.r = 10;
        this.max_stroke_width = 10;
        this.default_charge_strength = -1;

        //Create SVG element

        var svg = d3.select("td")
            .append("svg")
            .attr("width", this.w)
            .attr("height", this.h);

        // https://bl.ocks.org/mbostock/5e81cc677d186b6845cb00676758a339
        svg.call(d3.zoom().on("zoom", function() {
            svg.attr("transform", d3.event.transform)
        }));

        // Save this svg so that drawGraph() updates the existing graph instead of drawing an additional graph each time
        this.svg = svg;
    }

    drawGraph(genes_graph) {

        var self = this;

        var dataset = genes_graph;
        var nodes_edges_norm = Math.sqrt(dataset.edges.length == 0 ? 1 : (dataset.nodes.length / Math.sqrt(dataset.edges.length)));

        //Initialize a simple force layout, using the nodes and edges in dataset
        var force = d3.forceSimulation(dataset.nodes)
            .force("charge", d3.forceManyBody().strength(self.default_charge_strength * nodes_edges_norm)) // https://stackoverflow.com/a/51618519
            .force("link", d3.forceLink(dataset.edges))
            .force("center", d3.forceCenter().x(self.w / 2).y(self.h / 2));

        var colors = d3.scaleOrdinal(d3.schemeCategory10);

        //Create edges as lines
        var edges = this.svg.selectAll("line")
            .data(dataset.edges)
            .enter()
            .append("line")
            .style("stroke", "#ccc")
            .style("stroke-width", function (d, i) {
                if (dataset.edges[i].corr <= self.corr_cutoff) {
                    return 0;
                }
                // return this.max_stroke_width;
                return dataset.edges[i].corr * self.max_stroke_width;
            });
            // .style("stroke-width", 1);

        //Create nodes as circles
        var nodes = this.svg.selectAll("circle")
            .data(dataset.nodes)
            .enter()
            .append("circle")
            .attr("r", self.r)
            .style("fill", function (d, i) {
                return colors(i);
            })
            .on("mouseover", function (d, i) {
                console.log(`Mouseover: ${i}`);
                self.handleCallback(i);
            })
            .call(d3.drag()  //Define what to do on drag events
                .on("start", dragStarted)
                .on("drag", dragging)
                .on("end", dragEnded));

        //Add a simple tooltip
        nodes.append("title")
            .text(function (d) {
                return d.name;
            });

        //Every time the simulation "ticks", this will be called
        // Prevent nodes from leaving the SVG canvas
        // https://bl.ocks.org/mbostock/1129492
        force.on("tick", function () {

            edges.attr("x1", function (d) {
                return d.source.x;
            })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            nodes.attr("cx", function (d) {
                    return d.x;
                })
                .attr("cy", function (d) {
                    return d.y;
                });

        });

        //Define drag event functions
        function dragStarted(d) {
            if (!d3.event.active) force.alphaTarget(0.01).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragging(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragEnded(d) {
            if (!d3.event.active) force.alphaTarget(0.01);
            d.fx = null;
            d.fy = null;
        }
    }

    eraseGraph(svg = this.svg) {
        // https://stackoverflow.com/a/10911546
        if (svg) {
            svg.selectAll("*").remove();
        }
    }

    render() {
        return (
            <div id={"#" + this.props.id}>
            </div>
        );
    }
}

export default Graph;