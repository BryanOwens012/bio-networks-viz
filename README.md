# bio-networks-viz

## Biological Co-Expression Network Visualizer

Senior Project by [Bryan Owens](https://www.linkedin.com/in/bryanowens012/), B.S. in computer science at Yale, May 2021

https://bio-networks-viz.web.app/

### About

Many microRNAs have a strong co-expression relationship with each other, meaning they synthesize proteins similarly — or more generally, their effects on organisms are correlated. This web app lets you visualize such co-expression correlations between microRNAs.

Although there are existing gene co-expression network (GCN) visualizers, my goal was to build my own interactive and user-friendly interface.

This app is hosted on Firebase (Google Cloud). It is preloaded with a microRNA co-expression network created from real cancer data, courtesy of my classmate Ciaran Hassan's Senior Thesis (*A Network-based Approach for the Derivation of Prognostic microRNA Signatures and Oncological Risk Scores*, by Hassan, Kumar, Lin, and Gerstein).

Note that in the source code, I use the terms "microRNA" and "gene" interchangeably. They are technically different biological entities, but since this project can visualize the correlations between any generic objects, you can treat them the same.

### Compilation

This program uses React to build and deploy to production on the hosting service Firebase. However, you can also run on the dev environment (on localhost).

`$ npm install`

`$ npm start`

To deploy to production (since you don't have the credentials to my Firebase account, you'll have to push to your own Firebase account):

`$ npm install`

`$ npm install --save firebase`

`$ firebase init hosting`

`$ npm run build`

`$ firebase deploy`

### Changing the genes dataset to something other than the preloaded one

To do this, you'll have to do some manual work and then restart/rebuild/redeploy the app.

1. Receive the new dataset as a CSV file that is tab-delimited, with column names \[gene1, gene2, pearson_correlation\]. Rename that dataset to `genes.csv` and move it into `src/`.
2. Run `src/csv_to_json.ipynb`, which will output the updated `genes.json`. This run can take up to a few minutes for CSV files larger than 100MB.
3. Now, restart/rebuild/redeploy the app, and it will contain the updated dataset.
